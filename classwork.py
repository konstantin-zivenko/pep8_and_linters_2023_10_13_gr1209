# Напишите приложение, которое переворачивает все слова входного текста:
# Пример "abcd efgh" => "dcba hgfe"
#
# Все небуквенные символы должны оставаться на прежних местах:
#
# Пример "a1bcd efg!h" => "d1cba hgf!e"
# Используйте латиницу только для тестирования.
#
# Вам следует написать функцию, которая возвращает обратный текст


def tricky_invert_text(text: str) -> str:
    """
    Reverses the words in the text.

    Each word remains in its place, and the order of symbols in words is reversed
    for letter symbols. Non-alphanumeric characters remain in place.
    :param text: str, required
    :return: str
    """
    result_list = []
    words = text.split()
    for word in words:
        invert_word = tricky_invert_word(word)
        result_list.append(invert_word)
    return " ".join(result_list)


def tricky_invert_word(word: str) -> str:
    """

    :param word: str, required
    :return: str
    """
    alphas = [symbol for symbol in word if symbol.isalpha()]
    # alphas = []
    # for symbol in word:
    #     if symbol.isalpha():
    #         alphas.append(symbol)
    result = ""
    for symbol in word:
        if symbol.isalpha():
            result += alphas.pop()
        else:
            result += symbol

    return result


if __name__ == "__main__":
    cases = (
        ("", ""),
        ("a", "a"),
        ("asd", "dsa"),
        ("asd fgh", "dsa hgf"),
        ("asd    fgh", "dsa hgf"),
        ("abcd efgh", "dcba hgfe"),
        ("a1bcd efg!h", "d1cba hgf!e"),
        ("123!?", "123!?"),
        ("a1b2c3", "c1b2a3"),
    )

    for number, (arg, result) in enumerate(cases):
        assert (
            tricky_invert_text(arg) == result
        ), f"ERROR in test {number}! tricky_invert_text({arg}) = {tricky_invert_text(arg)}, but expected {result}"
